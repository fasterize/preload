/*exported frzPreload*/
/*jshint undef:false */
window['frzPreload'] = (function(doc, loc, nav) {
  var module = {},
      $url,
      $delayBeforePreload = 200,
      $preloadTimer,
      $preloadMethod,
      $urlToPreload,
      $isPreloading,
      $currentLocationWithoutHash = removeHash(loc.href),
      $ua = nav.userAgent,
      $existingPreloadLink;

  function removeHash(url) {
    var index = url.indexOf('#');
    if (index < 0) {
      return url;
    }
    return url.substr(0, index);
  }

  function getLinkTarget(target) {
    while (target.nodeName !== 'A') {
      target = target.parentNode;
    }
    return target;
  }

  function preload() {
    var url = $urlToPreload;
    if (url === $url) {
      return;
    }

    addPreloadLink(url);
    sendPreloadLinkStat(addPreloadItemToSessionStorage(url));

    if ($preloadTimer) {
      clearTimeout($preloadTimer);
      $preloadTimer = false;
    }
    $isPreloading = true;
  }

  function addPreloadLink(url) {
    var hint = doc.createElement('link');
    hint.setAttribute('name', 'frz_preload');
    hint.setAttribute('rel', $preloadMethod);
    hint.setAttribute('href', url);
    doc.getElementsByTagName('head')[0].appendChild(hint);
  }

  function abortPreload() {
    var element = doc.getElementsByName('frz_preload')[0];
    if (element) {
      removePreloadItemFromSessionStorage(element.href);
      element.parentNode.removeChild(element);
    }
  }

  function mouseOver(e) {
    var a = getLinkTarget(e.target);
    a.addEventListener('mouseout', mouseOut);

    $preloadTimer = setTimeout(function () {
      var element = doc.getElementsByName('frz_preload')[0];

      if ($existingPreloadLink || (element && element.href !== a.href)) {
        abortPreload();
      }

      if (!element || element.href !== a.href) {
        $urlToPreload = a.href;
        preload();
      }
    }, $delayBeforePreload);
  }

  function mouseOut() {
    if ($preloadTimer) {
      clearTimeout($preloadTimer);
      $preloadTimer = false;
      return;
    }
  }

  function isIEUpTo11() {
    var regex = /(?:\b(MS)?IE\s+|\bTrident\/7\.0;.*\s+rv:)(\d+)/g;
    var match = regex.exec($ua);
    if (match && match[2]) {
      return parseInt(match[2], 10) >= 11;
    }
    return false;
  }

  function isChromeUpTo13() {
    var regex = /(Chrome\/)(\d+)/g;
    var match = regex.exec($ua);
    if (match && match[2]) {
      return parseInt(match[2], 10) >= 13;
    }
    return false;
  }

  function isPrerenderSupported() {
    return isChromeUpTo13() || isIEUpTo11();
  }

  function isSessionStorageSupported() {
    try {
      return 'sessionStorage' in window && window['sessionStorage'] !== null;
    } catch (e) {
      return false;
    }
  }

  function generateUUID() {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
      var r = Math.random()*16|0, v = c == 'x' ? r : (r&0x3|0x8);
      return v.toString(16);
    });
  }

  function addPreloadItemToSessionStorage(url) {
    var preloadItem = {
          'uuid': generateUUID(),
          'url': url,
          'referer': $url,
          'status': 'preloading',
          'method': $preloadMethod
        };
    if (isSessionStorageSupported()) {
      sessionStorage.setItem('preload-' + url, JSON.stringify(preloadItem));
    }
    return preloadItem;
  }

  function sendPreloadLinkStat(preloadItem) {
    if (window['BOOMR_GLOBAL_CONFIG'] !== undefined) {
      var img = new Image();
      img.src = BOOMR_GLOBAL_CONFIG.beacon_url + '?p_id=' + preloadItem.uuid +
                        '&p_status=link' +
                        '&p_method=' + $preloadMethod +
                        '&u=' + preloadItem.url +
                        '&r=' + preloadItem.referer +
                        '&cust=' + FRZ_GLOBAL_CUSTOMER_KEY;
    }
  }

  function removePreloadItemFromSessionStorage(url) {
    if (isSessionStorageSupported()) {
      var preloadItem = JSON.parse(sessionStorage.getItem('preload-' + url));
      preloadItem.status = 'aborted';
      sessionStorage.setItem('preload-' + url, JSON.stringify(preloadItem));
    }
  }

  function addMouseOverListernerOnLinks() {
    var as = doc.getElementsByTagName('a'),
        a;
    for (var i = 0; i < as.length; i++) {
      a = as[i];
      if (a.target || // target="_blank" etc.
          a.hasAttribute('download') ||
          (a.href.indexOf(loc.protocol + '//' + loc.host + '/') !== 0 &&  a.href.indexOf('/') !== 0) || // Another domain, or no href attribute, and not relative url
          (a.href.indexOf('#') > -1 && removeHash(a.href) === $currentLocationWithoutHash) // Anchor
         ) {
        continue;
      }
      a.addEventListener('mouseover', mouseOver);
    }
  }

  module.initPreload = function(userAgent) {
    if (loc.protocol === "file:") {
      return;
    }

    $url = loc.href;
    $preloadMethod = 'prefetch';
    if (userAgent) {
      $ua = userAgent;
    }
    if (isPrerenderSupported()) {
      $preloadMethod = 'prerender';
    }

    $existingPreloadLink = doc.getElementsByName('frz_preload')[0];
    if ($existingPreloadLink) {
      var preloadItem = addPreloadItemToSessionStorage($existingPreloadLink.href);
      sendPreloadLinkStat(preloadItem);
    } else {
      addMouseOverListernerOnLinks();
    }
  };

  module.initPreload(null);

  return module;

}(document,location,navigator));
