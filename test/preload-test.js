describe('preload', function(){
  var userAgent,
      preloadHref,
      a;

  beforeEach(function(){
    userAgent = 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/35.0.1916.153 Safari/537.36';
    preloadHref = 'http://localhost:9876/test';
    a = addElementA(preloadHref);
  });

  afterEach(function(){
    addMouseEvent(a, 'mouseout');
    removeElement(a);
    removeElement(document.getElementsByName("frz_preload")[0]);
    removePreloadItem();
  });

  it('should add a prerender link when mouseover <a> during more than 100 milliseconds on Chrome 13 and next versions', function(done){
    testPreload(userAgent, 300, 'prerender', 'preloading', done);
  });

  it('should add a prerender link when mouseover <a> during more than 100 milliseconds on IE 11 and next versions', function(done){
    userAgent = 'Mozilla/5.0 (Windows NT 6.1; Trident/7.0; rv:11.0) like Gecko';
    testPreload(userAgent, 300, 'prerender', 'preloading', done);
  });

  it('should add a prefetch link when mouseover <a> during more than 100 milliseconds on Chrome 12 and previous versions', function(done){
    userAgent = 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/12.0.1916.153 Safari/537.36';
    testPreload(userAgent, 300, 'prefetch', 'preloading', done);
  });

  it('should add a prefetch link when mouseover <a> during more than 100 milliseconds on IE 10 and previous versions', function(done){
    userAgent = 'Mozilla/5.0 (compatible; MSIE 10.0; Windows NT 6.1; WOW64; Trident/6.0)';
    testPreload(userAgent, 300, 'prefetch', 'preloading', done);
  });

  it('should add a prefetch link when mouseover <a> during more than 100 milliseconds on Firefox', function(done){
    userAgent = 'Mozilla/5.0 (Windows NT 5.1; rv:31.0) Gecko/20100101 Firefox/31.0';
    testPreload(userAgent, 300, 'prefetch', 'preloading', done);
  });

  it('should not add preload link before a wait time of 100 milliseconds', function(done){
    testPreload(userAgent, 10, null, null, done);
  });

  it('should add a preload link when the href is a relative path', function(done){
    preloadHref = a.href = "http://localhost:9876/page2";
    testPreload(userAgent, 300, 'prerender', 'preloading', done);
  });

  it('should not add a preload link when the href is an anchor', function(done){
    preloadHref = a.href = "#about";
    testPreload(userAgent, 300, 'nopreload', null, done);
  });

  it('should not add a preload link when a element has a target attribute', function(done){
    a.setAttribute("target", "_blank");
    testPreload(userAgent, 300, 'nopreload', null, done);
  });

  it('should not add a preload link when a element has a download attribute', function(done){
    a.setAttribute("download");
    testPreload(userAgent, 300, 'nopreload', null, done);
  });

  it('should not add a preload link when href links to a different domain', function(done){
    preloadHref = a.href = "http://www.google.com";
    testPreload(userAgent, 300, 'nopreload', null, done);
  });

  it('should not remove preload link when the mouse move out of the link preloaded', function(done){
    var hint = document.createElement('link');
    hint.setAttribute('name', 'frz_preload');
    hint.setAttribute('rel', 'prerender');
    hint.setAttribute('href', preloadHref);

    testPreload(userAgent, 300, 'prerender', 'preloading', function() {
      addMouseEvent(a, 'mouseout');
      var preloadElem = document.getElementsByName("frz_preload")[0],
          preloadItem = JSON.parse(sessionStorage.getItem('preload-' + preloadHref));

      expect(preloadElem.href).to.contain(preloadHref);
      expect(preloadItem.url).to.contain(preloadHref);
      expect(preloadItem.status).to.be('preloading');
      done();
    });
  });

  it('should update the preload link when the mouse move over a new link', function(done){
    preloadHref2 = 'http://localhost:9876/test2';
    secondA = addElementA(preloadHref2);

    var hint = document.createElement('link');
    hint.setAttribute('name', 'frz_preload');
    hint.setAttribute('rel', 'prerender');
    hint.setAttribute('href', preloadHref2);

    testPreload(userAgent, 300, 'prerender', 'preloading', function() {
      addMouseEvent(a, 'mouseout');
      addMouseEvent(secondA, 'mouseover');
      setTimeout(function () {
        var preloadElem = document.getElementsByName("frz_preload")[0],
            preloadItem = JSON.parse(sessionStorage.getItem('preload-' + preloadHref2));

        expect(preloadElem.href).to.contain(preloadHref2);
        expect(preloadItem.url).to.contain(preloadHref2);
        expect(preloadItem.status).to.be('preloading');
        done();
      }, 300);
    });
  });

  it('should add preloadItem and don\'t add mouseover listener on links when preload link is already in the page', function(done){
    var hint = document.createElement('link');
    hint.setAttribute('name', 'frz_preload');
    hint.setAttribute('rel', 'prerender');
    hint.setAttribute('href', preloadHref);
    document.getElementsByTagName('head')[0].appendChild(hint);
    testPreload(userAgent, 300, 'prerender', 'preloading', function() {
      addMouseEvent(a, 'mouseout');
      var preloadElem = document.getElementsByName("frz_preload")[0],
          preloadItem = JSON.parse(sessionStorage.getItem('preload-' + preloadHref));
      expect(preloadElem).to.be(hint)
      expect(preloadItem.url).to.contain(preloadHref);
      expect(preloadItem.status).to.be('preloading');
      done();
    });
  });

  function testPreload(userAgent, timeout, preloadMethod, preloadStatus, done) {
    frzPreload.initPreload(userAgent);
    addMouseEvent(a, 'mouseover');
    setTimeout(function(){
      var preloadElem = document.getElementsByName("frz_preload")[0],
          preloadItem = JSON.parse(sessionStorage.getItem('preload-' + preloadHref));
      if (timeout < 100 || preloadMethod === 'nopreload') {
        expect(preloadElem).to.be(undefined)
        expect(preloadItem).to.be(null);
      } else {
        expect(preloadElem.href).to.contain(preloadHref);
        expect(preloadElem.rel).to.be(preloadMethod);
        expect(preloadItem.url).to.contain(preloadHref);
        expect(preloadItem.status).to.be(preloadStatus);
      }
      done();
    }, timeout);
  }

  function addElementA(href) {
    var elem = document.createElement("a");
    elem.setAttribute("href", href);
    document.getElementsByTagName("body")[0].appendChild(elem);
    return elem;
  }

  function addMouseEvent(elem, event) {
    var evObj = document.createEvent('MouseEvents');
    evObj.initEvent( event, true, false );
    elem.dispatchEvent(evObj);
  }

  function removeElement(elem) {
    if (elem) {
      elem.parentNode.removeChild(elem);
    }
  }

  function removePreloadItem() {
    sessionStorage.removeItem('preload-' + preloadHref);
  }

});
