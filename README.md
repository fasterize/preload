# preload

Preload ressources just before the user click on the links, when mouse is over the link.
It uses [prerendering](https://developers.google.com/chrome/whitepapers/prerender)/[prefetching](https://developer.mozilla.org/en-US/docs/Web/HTTP/Link_prefetching_FAQ) browser features, depending on the user's browser :
 - Prerendering :
  - Chrome >= 13
  - IE >= 11
 - Prefetching :
  - all other browsers/versions

## Usage

```html
<!DOCTYPE html>
<html>
  <head>
    <title>preload</title>
  </head>
  <body>
    <a href="http://www.fasterize.com/what">test</a>
    <script src="preload.min.js"></script>
  </body>
</html>
```

Script should be included just before the closing `</body>` element.

When the user pass the mouse over the link, the DOM will look like this (on browsers supporting prerendering) :
```html
<!DOCTYPE html>
<html>
  <head>
    <title>preload</title>
    <link rel="prerender" href="http://www.fasterize.com/what" />
  </head>
  <body>
    <a href="http://www.fasterize.com/what">test</a>
    <script src="preload.min.js"></script>
  </body>
</html>
```

## Testing

```shell
npm install -g grunt-cli
npm install
grunt test
```

[Tests](test/) are written with [mocha](https://github.com/visionmedia/mocha).

## Building

[Install](http://code.google.com/p/closure-compiler/downloads/list) closure-compiler.

```bash
npm install
CLOSURE_PATH=~/path/to/closure-compiler/ grunt
```

You get the [build/preload.min.js](build/preload.min.js) file.

