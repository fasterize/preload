module.exports = function(grunt) {
  grunt.loadNpmTasks('grunt-closure-compiler');
  grunt.loadNpmTasks('grunt-contrib-jshint');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-release');
  grunt.loadNpmTasks('grunt-karma');

  var plugins = ['karma-mocha', 'karma-phantomjs-launcher'];
  var browsers = [/*'Chrome', 'Firefox',*/ 'PhantomJS'];

  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),

    karma: {
      options: {
        browsers: browsers,
        files: [
          'build/<%= pkg.name %>.min.js',
          'node_modules/expect.js/index.js',
          'test/**/*.js'
        ],
        frameworks: ['mocha'],
        plugins: plugins
      },
      single: {
        singleRun: true
      },
      dev: {
        reporters: 'dots',
        background: true
      },
      auto: {
        autoWatch: true
      }
    },

    watch: {
      tests: {
        files: 'test/**/*.js',
        tasks: ['karma:dev:run']
      }
    },

    'closure-compiler': {
      min: {
        js: '<%= pkg.name %>.js',
        jsOutputFile: 'build/<%= pkg.name %>.min.js',
        options: {
          compilation_level: 'ADVANCED_OPTIMIZATIONS',
          externs: 'external.js'
        }
      }
    },

    jshint: {
      files: ['<%= pkg.name %>.js', 'package.json'],
      options: {
        jshintrc: '.jshintrc'
      }
    },

    release: {
      options: {
        npmtag: true
      }
    }
  });

  grunt.registerTask('test', ['karma:single']);

  grunt.registerTask('default', [
    'jshint',
    'closure-compiler',
    'test',
  ]);

};
